import pandas as pd
from snakemake.utils import validate, min_version

##### set minimum snakemake version #####
min_version("5.1.2")


##### load config and sample sheets #####

configfile: "config.yaml"
validate(config, schema="schemas/config.schema.yaml")

samples = pd.read_table(config["samples"]).set_index("sample", drop=False)
validate(samples, schema="schemas/samples.schema.yaml")

units = pd.read_table(config["units"], dtype=str).set_index(["sample", "unit"], drop=False)
units.index = units.index.set_levels([i.astype(str) for i in units.index.levels])  # enforce str in index
validate(units, schema="schemas/units.schema.yaml")

#for itt in units.iterrows():
#   print(itt)
#   #print(getattr(itt, 'sample'),getattr(itt, 'sample'))


##### target rules #####

rule all:
    input:
        expand(["{resDir}/results/clustering/{contrast}.clustering.tsv",
                "{resDir}/results/clustering/{contrast}.ma-plot.svg"],
               contrast=config["clustering"]["contrasts"],
               resDir  =config["resultsDirectory"]),
        expand("{resDir}/results/pca.svg",resDir  =config["resultsDirectory"])


##### setup singularity #####

# this container defines the underlying OS for each job when using the workflow
# with --use-conda --use-singularity
singularity: "docker://continuumio/miniconda3"


##### setup report #####

report: "report/workflow.rst"


##### load rules #####

include: "rules/common.smk"

if config["barcodeAssignment"]:
   include: "rules/barcodeAssignment.smk"

include: "rules/fixBarcode.smk"
include: "rules/trim.smk"
include: "rules/align.smk"
include: "rules/aktionT4.smk"
include: "rules/indexToRG.smk"
include: "rules/tagDup.smk"
include: "rules/coverage.smk"
include: "rules/clustering.smk"
#include: "rules/bam2matrix.smk"
include: "rules/htscount.smk"
include: "rules/clustering.smk"
