
rule trimmomatic_pe:
    input:
        unpack(get_fixbarcode)
    output:
        r1="{resDir}/{sample}-{unit}/trimmed/{sample}-{unit}.1.fastq.gz",
        r2="{resDir}/{sample}-{unit}/trimmed/{sample}-{unit}.2.fastq.gz",
        # reads where trimming entirely removed the mate
        r1_unpaired="{resDir}-{unit}/{sample}/trimmed/{sample}-{unit}.1.unpaired.fastq.gz",
        r2_unpaired="{resDir}-{unit}/{sample}/trimmed/{sample}-{unit}.2.unpaired.fastq.gz",
        trimlog="{resDir}/{sample}-{unit}/trimmed/{sample}-{unit}.trimlog"
    log:
        "{resDir}/{sample}-{unit}/logs/trimmomatic/{sample}-{unit}.log"
    params:
        # list of trimmers (see manual)
        trimmer= config["params"]["trimmomatic"]["trimmer"],
        # decorating optional parameters
        extra=lambda w, output: "-trimlog {0}".format(output.trimlog),
    message: "Trimming adapters on {input}"
    wrapper:
        "0.31.1/bio/trimmomatic/pe"

