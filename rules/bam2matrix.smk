
rule fixbarcode:
    input:
        unpack(get_fastq)
    output:
        fastq1="data/{sample}/fixbarcode/{sample}-{unit}.1.fastq.gz",
        fastq2="data/{sample}/fixbarcode/{sample}-{unit}.2.fastq.gz",
    conda:
        "../envs/bam2matrix.yaml"
    shell:
        "scripts/sc_atac_bam2matrix.py -B -I -O -P -C -W "

