rule indexToRG:
    input:
        unpack(get_indexTable),
        bam = "{resDir}/{sample}-{unit}/mapped/{sample}-{unit}.filter.sort.bam",
        bai = "{resDir}/{sample}-{unit}/mapped/{sample}-{unit}.filter.sort.bam.bai"
    output:
        "{resDir}/{sample}-{unit}/mapped/{sample}-{unit}.filter.sort.rg.bam"
    conda:
        "../envs/indexToRG.yaml"
    threads: 1
    script:
        "../scripts/sc_index_to_RG.py --input {input.bam} --index {input.indexTable} --output {output} --report"

