
rule barcodeAssignment:
    input:
        unpack(get_raw_fastq),
        unpack(get_machinBarcodes)
    output:
        fastq1=pipe("{resDir}/{sample}-{unit}/fixbarcode/{sample}-{unit}.withBarcode.1.fastq.gz"),
        fastq2=pipe("{resDir}/{sample}-{unit}/fixbarcode/{sample}-{unit}.withBarcode.2.fastq.gz")
    conda:
        "../envs/fixBarcodes.yaml"
    log:
        "{resDir}/{sample}-{unit}/fixbarcode/{sample}-{unit}.assign.log"
    message: "Assigning barcodes for files {input}."
    shell:
        "scripts/barcode_to_qname.py --Read1 {input.r1} --Read2 {input.r2} --output1 {output.fastq1} --output2 {output.fastq2} -I1 {input.I1} -I2 {input.I2} -I3 {input.I3} -I4 {input.I4} --nextseq > {log[0]}"

