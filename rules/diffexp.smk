
rule count_matrix:
    input:
        expand("{{resDir}}/{unit.sample}-{unit.unit}/mapped/{unit.sample}-{unit.unit}.tab", unit=units.itertuples())
    output:
        "{resDir}/results/counts/all.tsv"
    params:
        units=units
    conda:
        "../envs/pandas.yaml"
    script:
        "scripts/count-matrix.py"


def get_deseq2_threads(wildcards=None):
    # https://twitter.com/mikelove/status/918770188568363008
    few_coeffs = False if wildcards is None else len(get_contrast(wildcards)) < 10
    return 1 if len(samples) < 100 or few_coeffs else 6


rule deseq2_init:
    input:
        counts="{resDir}/results/counts/all.tsv"
    output:
        "{resDir}/results/deseq2/all.rds"
    params:
        samples=config["samples"]
    conda:
        "../envs/deseq2.yaml"
    log:
        "{resDir}/results/logs/deseq2/init.log"
    threads: get_deseq2_threads()
    script:
        "scripts/deseq2-init.R"


rule pca:
    input:
        "{resDir}/results/deseq2/all.rds"
    output:
        report("{resDir}/results/pca.svg", "../report/pca.rst")
    params:
        pca_labels=config["pca"]["labels"]
    conda:
        "../envs/deseq2.yaml"
    log:
        "{resDir}/results/logs/pca/pca.log"
    script:
        "scripts/plot-pca.R"


def get_contrast(wildcards):
    return config["diffexp"]["contrasts"][wildcards.contrast]


rule deseq2:
    input:
        "{resDir}/results/deseq2/all.rds"
    output:
        table=report("{resDir}/results/diffexp/{contrast}.diffexp.tsv", "../report/diffexp.rst"),
        ma_plot=report("{resDir}/results/diffexp/{contrast}.ma-plot.svg", "../report/ma.rst"),
    params:
        contrast=get_contrast
    conda:
        "../envs/deseq2.yaml"
    log:
        "{resDir}/results/logs/deseq2/{contrast}.diffexp.log"
    threads: get_deseq2_threads
    script:
        "scripts/deseq2.R"

