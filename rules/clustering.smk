
rule combine_cov:
    input:
        expand("{{resDir}}/{unit.sample}-{unit.unit}/mapped/{unit.sample}-{unit.unit}.filter.sort.rg.cov", unit=units.itertuples())
    output:
        "{resDir}/results/counts/all.tsv"
    params:
        samples= units["sample"].tolist(),
        units= units["unit"].tolist()
    conda:
        "../envs/pandas.yaml"
    script:
        "../scripts/count-matrix.py"


def get_mclust_threads(wildcards=None):
    # https://twitter.com/mikelove/status/918770188568363008
    few_coeffs = False if wildcards is None else len(get_contrast(wildcards)) < 10
    return 1 if len(samples) < 100 or few_coeffs else 6


rule mclust_init:
    input:
        counts="{resDir}/results/counts/all.tsv"
    output:
        "{resDir}/results/clustering/all.rds"
    params:
        samples=config["samples"]
    conda:
        "../envs/clustering.yaml"
    log:
        "{resDir}/results/logs/mclust/init.log"
    threads: get_mclust_threads()
    script:
        "../scripts/clustering-init.R"


rule pca:
    input:
        "{resDir}/results/clustering/all.rds"
    output:
        report("{resDir}/results/pca.svg", "../report/pca.rst")
    params:
        pca_labels=config["pca"]["labels"]
    conda:
        "../envs/clustering.yaml"
    log:
        "{resDir}/results/logs/pca/pca.log"
    script:
        "../scripts/plot-pca.R"


def get_contrast(wildcards):
    return config["clustering"]["contrasts"][wildcards.contrast]


rule clustering:
    input:
        "{resDir}/results/counts/all.tsv" #rds"
    output:
        table=report("{resDir}/results/clustering/{contrast}.clustering.tsv", "../report/clustering.rst"),
        ma_plot=report("{resDir}/results/clustering/{contrast}.ma-plot.svg", "../report/ma.rst"),
    params:
        contrast=get_contrast,
        cutoffVal=config["clustering"]["cutoff"]
    conda:
        "../envs/clustering.yaml"
    log:
        "{resDir}/results/logs/mclust/{contrast}.clustering.log"
    threads: get_mclust_threads
    script:
        "../scripts/mclust.R"

