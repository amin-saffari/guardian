rule htseq:
    input:
       bam = "{resDir}/{sample}-{unit}/mapped/{sample}-{unit}.filter.sort.rg.cov"
    output:
       "{resDir}/{sample}-{unit}/mapped/{sample}-{unit}.tab"
    conda:
       "../envs/htseq.yaml"
    shell:
       "htseq-count --format bam --type gene --idattr gene_id {input.bam} > {output}" #{params.gtf}  > {output}"
