rule dedups:
    input:
        bam="{resDir}/{sample}-{unit}/mapped/{sample}-{unit}.filter.sort.bam",
        bai="{resDir}/{sample}-{unit}/mapped/{sample}-{unit}.filter.sort.bam.bai"
    output:
        "{resDir}/{sample}-{unit}/mapped/{sample}-{unit}.filter.sort.dedups.bam"
    conda:
        "../envs/dedups.yaml"
    message: "Deduplication on files {input}"
    threads: 1
    script:
        "../scripts/sc_atac_true_dedup.py {input.bam} {output}"
