rule coverage:
    input:
        bam = "{resDir}/{sample}-{unit}/mapped/{sample}-{unit}.filter.sort.rg.bam"
    output:
        file = "{resDir}/{sample}-{unit}/mapped/{sample}-{unit}.filter.sort.rg.cov"
    conda:
        "../envs/coverage.yaml"
    message:
        "Calculating converage for each library"
    threads: 1
    shell:   "scripts/sc_count_RG.py --input {input.bam} --output {output.file}"
