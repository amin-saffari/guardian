##### Helper functions from different smk written by johanneskoester #####

def is_single_end(sample, unit, resDir):
    return pd.isnull(units.loc[(sample, unit), "fq2"])

def get_raw_fastq(wildcards):
    """Get fastq files of given sample-unit."""
    if config["barcodeAssignment"]:
       fastqs = units.loc[(wildcards.sample, wildcards.unit), ["fq1", "fq2"]].dropna()
       if len(fastqs) == 2:
          return {"r1": fastqs.fq1, "r2": fastqs.fq2}
       return {"r1": fastqs.fq1}
    else:
       exit("barcodeAssignment is not true")


def get_fastq(wildcards):
    """Get fastq files of given sample-unit."""
    if config["barcodeAssignment"]:
        paires=["r1","r2"]
        fastqs = dict(zip(paires,expand("{resDir}/{sample}-{unit}/fixbarcode/{sample}-{unit}.withBarcode.{group}.fastq.gz",
                      group=[1, 2], **wildcards)))
        return fastqs
    else:
        fastqs = units.loc[(wildcards.sample, wildcards.unit), ["fq1", "fq2"]].dropna()
        if len(fastqs) == 2:
             return {"r1": fastqs.fq1, "r2": fastqs.fq2}
        return {"r1": fastqs.fq1}

def get_barcodes(wildcards):
    """Get index files of given sample-unit."""
    barcodes = units.loc[(wildcards.sample, wildcards.unit), ["P7_i5", "P7_PCR", "P5_i5", "P5_PCR"]].dropna()
    #if len(barcodes) != 4:
    #    exit("Error ")
    return {"P7_i5": barcodes.P7_i5, "P7_PCR": barcodes.P7_PCR, "P5_i5": barcodes.P5_i5, "P5_PCR": barcodes.P5_PCR}
    return barcodes
    
def get_machinBarcodes(wildcards):
    """Get index files of given sample-unit."""
    barcodes = units.loc[(wildcards.sample, wildcards.unit), ["I1", "I2", "I3", "I4"]].dropna()
    #if len(barcodes) != 4:
    #    exit("Error ")
    return {"I1": barcodes.I1, "I2": barcodes.I2, "I3": barcodes.I3, "I4": barcodes.I4}
    return barcodes
    

def get_fixbarcode(wildcards):
    """Get fixed-barcoded fastq files of a given sample-unit."""
    paires=["r1","r2"]
    fastqs=[]
    if not is_single_end(**wildcards):
        # paired-end sample
        fastqs=expand("{resDir}/{sample}-{unit}/fixbarcode/{sample}-{unit}.{group}.fastq.gz",
                      group=[1, 2], **wildcards)
    else:
        # single end sample
        fastqs="{resDir}/{sample}-{unit}/fixbarcode/{sample}-{unit}.fastq.gz".format(**wildcards)
    return dict(zip(paires,fastqs))
    

def get_trimmed_reads(wildcards):
    """Get trimmed reads of given sample-unit."""
    if not is_single_end(**wildcards):
        # paired-end sample
        return expand("{resDir}/{sample}-{unit}/trimmed/{sample}-{unit}.{group}.fastq.gz",
                      group=[1, 2], **wildcards)
    # single end sample
    return "{resDir}/{sample}-{unit}/trimmed/{sample}-{unit}.fastq.gz".format(**wildcards)

def get_decoratedHisatInput(wildcards):
   # Input file wrangling
   print("-1 {}".format(**wildcards))
   if not is_single_end(**wildcards):
      # paired-end sample
      print("-1 {0} -2 {0}".format(*wildcards))
      return "{resDir}/{sample}-{unit}/trimmed/{sample}-{unit}.{group}.fastq.gz".format(*wildcards)
   # single end sample
   return "-U {0}".format(**wildcards)

def get_indexTable(wildcards):
    """Get barcode files of given sample-unit."""
    indextable = "{resDir}/{sample}-{unit}/fixbarcode/master.indextable".format(**wildcards)
    return {"indexTable": indextable}
