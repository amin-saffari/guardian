rule filtering:
    input:
        "{resDir}/{sample}-{unit}/mapped/{sample}-{unit}.bam"
    output:
        pipe("{resDir}/{sample}-{unit}/mapped/{sample}-{unit}.filter.sam")
    params:
        # optional parameters
        extra = " ".join(config["params"]["samtools"])
    message: "{wildcards.sample} is not in the sudoers file. This incident will be reported. More information, https://xkcd.com/838/"
    group: "AktionT4"
    threads: 1
    wrapper:
        "0.31.1/bio/samtools/view"

rule cleansing:
    input:
        "{resDir}/{sample}-{unit}/mapped/{sample}-{unit}.filter.sam"
    output:
        pipe("{resDir}/{sample}-{unit}/mapped/{sample}-{unit}.clean.sam")
    params:
        # optional parameters
        extra = config["params"]["filters"]
    message: "{wildcards.sample} is not in the sudoers file. This incident will be reported. More information, https://xkcd.com/838/"
    group: "AktionT4"
    threads: 1
    shell:
        "grep -vE '{params.extra}' {input} > {output}"

rule sorting:
    input:
        "{resDir}/{sample}-{unit}/mapped/{sample}-{unit}.clean.sam"
    output:
        "{resDir}/{sample}-{unit}/mapped/{sample}-{unit}.filter.sort.bam"
    message: "Sorting {input}"
    group: "AktionT4"
    threads: 1
    wrapper:
        "0.31.1/bio/samtools/sort"

rule indexing:
    input:
        "{resDir}/{sample}-{unit}/mapped/{sample}-{unit}.filter.sort.bam"
    output:
        "{resDir}/{sample}-{unit}/mapped/{sample}-{unit}.filter.sort.bam.bai"
    message: "indexing {input}"
    group: "AktionT4"
    wrapper:
        "0.31.1/bio/samtools/index"
