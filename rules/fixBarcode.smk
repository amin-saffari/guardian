
rule fixbarcode:
    input:
        unpack(get_fastq),
        unpack(get_barcodes)
    output:
        fastq1="{resDir}/{sample}-{unit}/fixbarcode/{sample}-{unit}.1.fastq.gz",
        fastq2="{resDir}/{sample}-{unit}/fixbarcode/{sample}-{unit}.2.fastq.gz",
        indexTable="{resDir}/{sample}-{unit}/fixbarcode/master.indextable"
    conda:
        "../envs/fixBarcodes.yaml"
    log:
        "{resDir}/{sample}-{unit}/fixbarcode/{sample}-{unit}.log"
    message: "fixing barcode for files {input}."
    threads: 4
    shell:
        "scripts/barcode_correction.py --Read1 {input.r1} --Read2 {input.r2} --output1 {output.fastq1} --output2 {output.fastq2} --cores {threads} --P7_i5 {input.P7_i5} --P7_PCR {input.P7_PCR} --P5_i5 {input.P5_i5} --P5_PCR {input.P5_PCR} --FullBarcodes {output.indexTable} --nextseq > {log[0]}"

