
rule align:
    input:
        unpack(get_trimmed_reads)
    output:
        # see hsiat2 manual for additional output files
        "{resDir}/{sample}-{unit}/mapped/{sample}-{unit}.bam"
        # add this to extra if you want it "{resDir}/{sample}/mapped/{sample}-{unit}.NovelSpliceSite"
    conda:
        "../envs/hisat2.yaml"
    log:
        "{resDir}/{sample}-{unit}/logs/hisat2/{sample}-{unit}.log"
    params:
        # path to hisat2 reference genome index
        idx=config["ref"]["index"],
        decorated = lambda wildcards, input: "-1 {0} -2 {1}".format(input[0],input[1]),
        # optional parameters
        extra = config["params"]["hisat2"]
    message:"Aligning {input}"
    threads: 10
    shell:   "hisat2 {params.extra} --threads {threads} -x {params.idx} {params.decorated} | samtools view -Sbh -o {output[0]}"
