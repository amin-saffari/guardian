rule tagDup:
    input:
        "{resDir}/{sample}-{unit}/mapped/{sample}-{unit}.filter.sort.rg.bam"
    output:
        bam="{resDir}/{sample}-{unit}/mapped/{sample}-{unit}.filter.sort.rg.dedup.bam",
        metrics="{resDir}/{sample}-{unit}/dedup/{sample}-{unit}.metrics.txt"
    params:
        " ".join(config["params"]["picard"]["markduplicate"])
    log:
        "{resDir}/{sample}-{unit}/logs/picard/dedup/{sample}-{unit}.log"
    wrapper:
        "0.31.1/bio/picard/markduplicates"
