#!/usr/bin/env python

import sys
import argparse
import gzip
import io
import subprocess
import Levenshtein
from multiprocessing import Process, Value, Lock, Queue
from functools import reduce
from itertools import product
from operator import add

def editcheck(barc,reflist):
   try:
      reflist[barc]
      eddist = '0'
   except KeyError:
      winner = '_CTF' + '_'*(len(barc)-4)
      winner_ed = 10
      runnerup_ed = 10
      for barcode in reflist.keys():
         curred = Levenshtein.distance(barc,barcode)
         if curred <= winner_ed:
            runnerup_ed = winner_ed
            winner = barcode
            winner_ed = curred
         if curred > winner_ed & curred < runnerup_ed:
            runnerup_ed = curred
      if winner_ed > 3:
         winner = '_CTF' + '_'*(len(barc)-4)
      if runnerup_ed - winner_ed < 2:
         winner = '_AMBIG' + '_'*(len(barc)-6)
      barc = winner
      eddist = str(winner_ed)
   return(barc,eddist)

complements = {'A':'T', 'T':'A', 'G':'C', 'C':'G','N':'N'}
def reverse_complement(x):
    xrev = x[::-1]
    xrevcomp = ''.join([complements[z] for z in xrev])
    return xrevcomp

def readBarcode(barcodefile):
    barcode = {}
    with open(barcodefile, 'r') as bf:
        for line in bf:
            key = line.strip().split()[0] #only the 1st column
            if key in barcode.keys():
                print("WARNING: {0} is not uniq in your file".format(key))
            else:
                barcode[key] = ""
    return barcode
  
def makeBarcodes(barcodes):
    """ Make all of your barcodes for a given order
    now in this order (P7_i5,P7_PCR,P5_PCR,P5_i5)
    """
    return dict(map(lambda x: (reduce(add,x,''),0), product(*(barcodes))))


class Counter(object):
    def __init__(self, initval=0):
        self.val = Value('i', initval)
        self.lock = Lock()

    def increment(self):
        with self.lock:
            self.val.value += 1

    def value(self):
        with self.lock:
            return self.val.value

class fqWorker(object):

    def __init__(self, numprocs, infile, outfile, nextSeq, fullBarcodes,P7_i5,P7_PCR,P5_PCR,P5_i5):
        # A template to represent every block of information in fastq format.
        self.bTemplate = {'ID': '', 'PI': '', 'SQ': '', 'OP': '+', 'QU': ''}
        self.totreads = Counter(0)
        self.exactmatch = Counter(0)
        self.editmatch = Counter(0)
        self.failed = Counter(0)
        self.P7_PCR=P7_PCR
        self.P7_i5=P7_i5
        self.P5_PCR=P5_PCR
        self.P5_i5=P5_i5
        self.nextSeq = nextSeq
        self.fullBarcodes= fullBarcodes
        self.numprocs = numprocs
        self.reverse_complement=reverse_complement
        self.editcheck=editcheck
        self.infile = open(infile, mode='r')
        self.outfile = outfile
        self.in_blocks = self.blockReader()
        self.inq = Queue()
        self.outq = Queue()

        self.pin  = Process(target=self.parse_input_fq, args=())
        self.pout = Process(target=self.write_output_fq, args=())
        self.ps   = [ Process(target=self.produceHeat, args=())
                        for i in range(self.numprocs)]

        self.pin.start() # start labeling in memory blocks
        self.pout.start() # start writing processed blocks
        for p in self.ps: # start produce heat
            p.start()

        self.pin.join()
        i = 0
        for p in self.ps:
            p.join()
            print("Done", i)
            i += 1

        self.pout.join()
        self.infile.close()

    def blockReader(self):

      """ Process block as every four lines 
      """
      blocks=[]
      #if(lengt(blocks) < 1):
      for line in self.infile:
            block = self.bTemplate.copy()
            seqbarc = line.strip().split()[1].split(':')[0]
            seqPair = line.strip().split('/')[-1]
            if self.nextSeq:
                seqbarc = seqbarc[0:18] + reverse_complement(seqbarc[18:28]) + reverse_complement(seqbarc[28:36]) # [8bp Nextera N7 barcode] + [10bp PCR P7 barcode] + [10bp PCR P5 barcode] + [8bp Nextera N5 barcode] 
            block['ID'] = seqbarc
            block['PI'] = seqPair
            block['SQ'] = next(self.infile).strip()
            block['OP'] = next(self.infile).strip()
            block['QU'] = next(self.infile).strip()
            numReads += 1
            blocks.append(block)
          
      return blocks

      
    def parse_input_fq(self):
            """Parses the input fastq and yields tuples with the index of the block
            as the first element, and the fileds of the block as the second
            element.

            The index is zero-index based.

            The data is then sent over inqueue for the workers to do their
            thing.  At the end the input process sends a 'STOP' message for each
            worker.
            """

            for i, block in enumerate(self.in_blocks):
                self.inq.put( (i, block) )

            for i in range(self.numprocs):
                self.inq.put("STOP")

    def produceHeat(self):
        """
        Workers. Consume inq and produce answers on outq
        """
        for i, block in iter(self.inq.get, "STOP"):
               self.totreads.increment()
               seqed = "0000"
               try:
                  self.fullBarcodes[block['ID']]
                  self.exactmatch.increment()
               except KeyError:
                   #TODO: The next 8 lines must be converted into zip
                   b1 = block['ID'][0:8]
                   b2 = block['ID'][8:18]
                   b3 = block['ID'][18:28]
                   b4 = block['ID'][28:36]
                   b1ed = self.editcheck(b1,self.P7_i5)
                   b2ed = self.editcheck(b2,self.P7_PCR)
                   b3ed = self.editcheck(b3,self.P5_PCR)
                   b4ed = self.editcheck(b4,self.P5_i5)
                   block['ID'] = b1ed[0] + b2ed[0] + b3ed[0] + b4ed[0]
                   seqed = b1ed[1] + b2ed[1] + b3ed[1] + b4ed[1]
                   #print(block['ID'])
                   if 'CTF' in block['ID'] or 'AMBIG' in block['ID']:
                      self.failed.increment()
                   else:
                      self.editmatch.increment()
               block['ID'] = "@{0}:{1}#{2}".format(block['ID'],str(i),seqed) 
               self.outq.put( (i, block) )
        self.outq.put("STOP")

    def write_output_fq(self):
        """
        Open outgoing fq file then start reading outq for answers
        Since I chose to make sure output was synchronized to the input there
        is some extra goodies to do that.

        Obviously your input has the original block-index number so this is not
        required.
        """
        cur = 0
        stop = 0
        buffer = {}
        # For some reason writer works badly across processes so open/close
        # and use it all in the same process or else you'll have the last
        # several rows missing
        outfile = gzip.GzipFile(self.outfile, mode='wb')
        #Keep running until we see numprocs STOP messages
        for works in range(self.numprocs):
            for i, block in iter(self.outq.get, "STOP"):
                # verify rows are in order, if not save in buffer
                if i != cur:
                    buffer[i] = block
                else:
                    #if yes are write it out and make sure no waiting rows exist
                    outfile.write(("{ID}/{PI}\n{SQ}\n{OP}\n{QU}\n".format(**block)).encode('UTF-8'))
                    cur += 1
                    while cur in buffer:
                        outfile.write(("{ID}/{PI}\n{SQ}\n{OP}\n{QU}\n".format(**(buffer[cur])).encode('UTF-8')))
                        del buffer[cur]
                        cur += 1

        outfile.close()
        print("total={0} exact={1:.2f}% by_ed={2:.2f}% fail={3:.2f}%".format(self.totreads.value(), round(float(self.exactmatch.value())*100/self.totreads.value(),2), round(float(self.editmatch.value())*100/self.totreads.value(),2), round(float(self.failed.value())*100/self.totreads.value(),2)))


def correctHuman(argv):
    parser = argparse.ArgumentParser(description='A program to fix erroneous barcodes in scATAC data. The current order is (P7_i5,P7_PCR ,P5_PCR,P5_i5)')
    parser.add_argument('-1','--Read1', help='Input fastq file 1',dest='Read1',required=True)
    parser.add_argument('-2','--Read2', help='Input fastq file 2',dest='Read2',required=True)
    parser.add_argument('-O1','--output1', help='Output fastq file 1',dest='output1',required=True)
    parser.add_argument('-O2','--output2', help='Output fastq file 2',dest='output2',required=True)
    parser.add_argument('-c','--cores',type=int, help='The number of processes for every input file [DEFAULT: %default]',dest='cores', default=1)
    parser.add_argument('-i7','--P7_i5', help='P7_i5 Full barcodes of the experiment',dest='i7',required=True)
    parser.add_argument('-p7','--P7_PCR', help='P7_PCR Full barcodes of the experiment',dest='p7',required=True)
    parser.add_argument('-i5','--P5_i5', help='P5_i5 Full barcodes of the experiment',dest='i5',required=True)
    parser.add_argument('-p5','--P5_PCR', help='P5_PCR Full barcodes of the experiment',dest='p5',required=True)
    parser.add_argument('-b','--FullBarcodes', help='Output file for the list of full barcodes.',dest='fullbarcodes',required=True)
    parser.add_argument('-X','--nextseq',help='NextSeq run indicator',dest='nextseq',action="store_true")
    args = parser.parse_args(argv)
    return args


def writefullBarcode(fName,fullBarcodes):
    with open(fName, mode='w') as fdBarcodes:
        fdBarcodes.writelines('{0}\t{1}\n'.format(k,v) for k, v in fullBarcodes.items())

def main(argv):
    args = correctHuman(argv)
    P7_i5, P7_PCR, P5_PCR, P5_i5=list(map(readBarcode,[args.i7,args.p7,args.p5,args.i5]))
    if set(map(len, P7_i5)) != {8}:
        sys.exit("P7_i5 must contains barcodes of length all 8")
        
    if set(map(len, P7_PCR)) != {10}:
        sys.exit("P7_PCR must contains barcodes of length all 10")
    
    if set(map(len, P5_PCR)) != {10}:
        sys.exit("P5_PCR must contains barcodes of length all 10")
    
    if set(map(len, P5_i5)) != {8}:
        sys.exit("P5_i5 must contains barcodes of length all 8")
    fullBarcodes = makeBarcodes([P7_i5,P7_PCR,P5_PCR,P5_i5])
    print("Writing the fullbarcode")
    writefullBarcode(args.fullbarcodes,fullBarcodes)
    pool  = fqWorker(args.cores,args.Read2,args.output1,args.nextseq,fullBarcodes,P7_i5,P7_PCR,P5_PCR,P5_i5)
    pool2 = fqWorker(args.cores,args.Read2,args.output2,args.nextseq,fullBarcodes,P7_i5,P7_PCR,P5_PCR,P5_i5)

if __name__ == '__main__':
    main(sys.argv[1:])
