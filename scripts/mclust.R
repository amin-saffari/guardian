log <- file(snakemake@log[[1]], open="wt")
sink(log)
sink(log, type="message")

library("mclust")

parallel <- FALSE
if (snakemake@threads > 1) {
    library("BiocParallel")
    # setup parallelization
    register(MulticoreParam(snakemake@threads))
    parallel <- TRUE
}

cellfloor = snakemake@params[["cutoff"]]
nobkgdmat <- readRDS(snakemake@input[[1]])

# Use this it is important
# mclustBIC(X)

# Use Mclust with initialization to make he process faster

# To use parallel
# https://hpc-forge.cineca.it/files/CoursesDev/public/2012%20Autumn/script%20R/modelBased_genData.R

# Why G (numbers of mixture components (clusters)) is hard coded to be 2?
cellcall = Mclust(data.frame(log10(nobkgdmat$Total)),G=2)
if(cellfloor == "auto"){ 
	cellfloor = min(nobkgdmat[which(cellcall$classification == 2 & cellcall$uncertainty < 0.05),2]) 
}else{ 
	cellfloor = as.numeric(cellfloor)
}

subsetmat = nobkgdmat[which(nobkgdmat$Total >= cellfloor),]
res = cbind(as.character(rownames(subsetmat)),as.character(subsetmat[,1]))
# output.table is equal to .readdepth.cells.indextable.txt
write.table(res, snakemake@output[["table"]],row.names=FALSE,col.names=FALSE,sep="\t",quote=F)
# subsamples and its for loop can be handled by ggplot
subsamples = levels(nobkgdmat$Tag)

# Some ploting

dev.off()
