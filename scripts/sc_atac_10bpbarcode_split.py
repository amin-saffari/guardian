#!/usr/bin/env python

import sys
import argparse
import gzip
import io
import subprocess
import Levenshtein
#from itertools import imap

parser = argparse.ArgumentParser(description='A program to fix erroneous barcodes in scATAC data. The current order is (P7_i5,P7_PCR ,P5_PCR,P5_i5)')
parser.add_argument('-1','--Read1', help='Input fastq file 1',dest='Read1',required=True)
parser.add_argument('-2','--Read2', help='Input fastq file 2',dest='Read2',required=True)
parser.add_argument('-O1','--output1', help='Output fastq file 1',dest='output1',required=True)
parser.add_argument('-O2','--output2', help='Output fastq file 2',dest='output2',required=True)
parser.add_argument('-i7','--P7_i5', help='P7_i5 Full barcodes of the experiment',dest='i7',required=True)
parser.add_argument('-p7','--P7_PCR', help='P7_PCR Full barcodes of the experiment',dest='p7',required=True)
parser.add_argument('-i5','--P5_i5', help='P5_i5 Full barcodes of the experiment',dest='i5',required=True)
parser.add_argument('-p5','--P5_PCR', help='P5_PCR Full barcodes of the experiment',dest='p5',required=True)
parser.add_argument('-b','--FullBarcodes', help='Output file for the list of full barcodes.',dest='fullbarcodes',required=True)
#parser.add_argument('-b','--barcodes', help='Input Full barcodes of the experiment',dest='barcodes',required=True)
parser.add_argument('-X','--nextseq',help='NextSeq run indicator',dest='nextseq',action="store_true")
args = parser.parse_args()

def editcheck(barc,reflist):
   try:
      reflist[barc]
      eddist = '0'
   except KeyError:
      winner = '_CTF' + '_'*(len(barc)-4)
      winner_ed = 10
      runnerup_ed = 10
      for barcode in reflist.keys():
         curred = Levenshtein.distance(barc,barcode)
         if curred <= winner_ed:
            runnerup_ed = winner_ed
            winner = barcode
            winner_ed = curred
         if curred > winner_ed & curred < runnerup_ed:
            runnerup_ed = curred
      if winner_ed > 3:
         winner = '_CTF' + '_'*(len(barc)-4)
      if runnerup_ed - winner_ed < 2:
         winner = '_AMBIG' + '_'*(len(barc)-6)
      barc = winner
      eddist = str(winner_ed)
   return(barc,eddist)

complements = {'A':'T', 'T':'A', 'G':'C', 'C':'G','N':'N'}
def reverse_complement(x):
   xrev = x[::-1]
   xrevcomp = ''.join([complements[z] for z in xrev])
   return xrevcomp

nex_i7 = {}
pcr_i7 = {}
pcr_i5 = {}
nex_i5 = {}

def readBarcode(barcodefile):
    barcode = {}
    with open(barcodefile, 'r') as bf:
        for line in bf:
            key = line.strip().split()[0] #only the 1st column
            if key in barcode.keys():
                print("WARNING: {0} is not uniq in your {1}".format(key,barcodefile))
            else:
                barcode[key] = ""
    return barcode

nex_i7,pcr_i7,pcr_i5,nex_i5=list(map(readBarcode,[args.i7,args.p7,args.p5,args.i5]))

if set(map(len, nex_i7)) != {8}:
    sys.exit("P7_i5 must contains barcodes of length all 8")

if set(map(len, pcr_i7)) != {10}:
    sys.exit("P7_PCR must contains barcodes of length all 10")

if set(map(len, pcr_i5)) != {10}:
    sys.exit("P5_PCR must contains barcodes of length all 10")

if set(map(len, nex_i5)) != {8}:
    sys.exit("P5_i5 must contains barcodes of length all 8")

FULL_BARC = {}
for i in nex_i7.keys():
   for j in pcr_i7.keys():
      for k in pcr_i5.keys():
         for l in nex_i5.keys():
            currbarc = i + j + k + l
            FULL_BARC[currbarc] = ""
    
with open(args.fullbarcodes, mode='w') as fdBarcodes:
        fdBarcodes.writelines('{0}\t{1}\n'.format(k,v) for k, v in FULL_BARC.items())


totreads = 0
exactmatch = 0
editmatch = 0
failed = 0
buffLength = 100000

print("total\texact\tby_ed\tfail")
with gzip.GzipFile(args.output1, mode='wb') as of1, \
        gzip.GzipFile(args.output2, mode='wb') as of2, \
        gzip.open(args.Read1, mode='rb') as infasta1, \
        gzip.open(args.Read2, mode='rb') as infasta2:
            
            r1Buff = []
            r2Buff = []
            for line in infasta1:
               barcodes = line.decode('UTF-8').strip().split()[1].split(':')[0]
               #barcodes = line.decode('UTF-8').strip().split()[1].split(':')[0]
               seqbarc = barcodes.replace('-','')
               if args.nextseq:
                  seqbarca = seqbarc[0:18] #[8bp Nextera N7 barcode] + [10bp PCR P7 barcode] 
                  seqbarcb1 = reverse_complement(seqbarc[18:28]) # [10bp PCR P5 barcode] 
                  seqbarcb2 = reverse_complement(seqbarc[28:36]) # [8bp Nextera N5 barcode]
                  seqbarc = seqbarca + seqbarcb1 + seqbarcb2
               #print(seqbarc)
               read1 = next(infasta1).strip()
               plus = next(infasta1)
               qual1 = next(infasta1).strip()
               dumpbarc = infasta2.readline()
               read2 = infasta2.readline().strip()
               plus = infasta2.readline()
               qual2 = infasta2.readline().strip()
               seqed = '0000'
               try:
                  FULL_BARC[seqbarc]
                  totreads += 1
                  exactmatch += 1
               except KeyError:
                   b1 = seqbarc[0:8] # 8bp
                   #print(b1)
                   b2 = seqbarc[8:18] # 10bp
                   #print(b2)
                   b3 = seqbarc[18:28] # 10bp
                   #print(b3)
                   b4 = seqbarc[28:36] # 8bp
                   #print(b4)
                   b1ed = editcheck(b1,nex_i7) # 8bp
                   b2ed = editcheck(b2,pcr_i7) # 10bp
                   b3ed = editcheck(b3,pcr_i5) # 10bp
                   #print(pcr_i5)
                   #print(b3ed)
                   b4ed = editcheck(b4,nex_i5) # 8bp
                   seqbarc = b1ed[0] + b2ed[0] + b3ed[0] + b4ed[0]
                   seqed = b1ed[1] + b2ed[1] + b3ed[1] + b4ed[1]
                   totreads += 1
                   if 'CTF' in seqbarc or 'AMBIG' in seqbarc:
                      failed += 1
                   else:
                      editmatch += 1

               r1Buff.append('@'.encode('UTF-8') + seqbarc.encode('UTF-8') + ':'.encode('UTF-8') + str(totreads).encode('UTF-8') + '#'.encode('UTF-8') + seqed.encode('UTF-8') + '/1\n'.encode('UTF-8') + read1 + '\n+\n'.encode('UTF-8') + qual1 + '\n'.encode('UTF-8'))
               r2Buff.append('@'.encode('UTF-8') + seqbarc.encode('UTF-8') + ':'.encode('UTF-8') + str(totreads).encode('UTF-8') + '#'.encode('UTF-8') + seqed.encode('UTF-8') + '/2\n'.encode('UTF-8') + read2 + '\n+\n'.encode('UTF-8') + qual2 + '\n'.encode('UTF-8'))
               
               if len(r1Buff) >= buffLength:
                   of1.writelines(r1Buff)
                   of2.writelines(r2Buff)
                   print("{0}\t{1:.2f}%\t{2:.2f}%\t{3:.2f}%".format(totreads, round(float(exactmatch)*100/totreads,2), round(float(editmatch)*100/totreads,2), round(float(failed)*100/totreads,2)))
                   r1Buff.clear()
                   r2Buff.clear()

            if len(r1Buff) > 0: ## Left over data
                of1.writelines(r1Buff)
                of2.writelines(r2Buff)
                r1Buff.clear()
                r2Buff.clear()

print("{0}\t{1:.2f}%\t{2:.2f}%\t{3:.2f}%".format(totreads, round(float(exactmatch)*100/totreads,2), round(float(editmatch)*100/totreads,2), round(float(failed)*100/totreads,2)))

