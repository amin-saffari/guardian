import pandas as pd

counts = [pd.read_csv(f, usecols=['Barcode','Experiment','ReadCount'],sep='\t')
          for f in snakemake.input]

for t, sample, unit in zip(counts, snakemake.params.samples, snakemake.params.units):
    t['Sample'] = sample
    t['Unit'] = unit
    #t.columns = [sample,unit]
print(counts[0].count())
matrix = pd.concat(counts, axis=0, keys=['Sample','Unit','Barcode','Experiment'] ,sort=False)
print(matrix.head())
## collapse technical replicates
matrix = matrix.groupby(level=2).sum()
print(matrix)
matrix.to_csv(snakemake.output[0], sep="\t")
