#!/usr/bin/env python

import sys
import os
import pysam
import argparse

parser = argparse.ArgumentParser(description="A program to calculate a coverage of RG tag.")
parser.add_argument('-b', '--input', help='Input bam file', dest='input', required=True)
parser.add_argument('-o', '--output', help='Output location', dest='output', required=True)
args = parser.parse_args()

def chkIndex(fAdd):
    if not os.path.isfile("{}.bai".format(fAdd)):
        print("indexing bam file {}".format(fAdd))
        pysam.index(fAdd)
        if not os.path.isfile("{}.bai".format(fAdd)):
            sys.exit("samtools couldn't creat index file!")

header={}
totalct={}
with pysam.AlignmentFile(args.input, mode='rb') as inbam:
    try:
        header = inbam.header['RG']
    except KeyError:
        sys.exit('A bam file with no RG!')
    chkIndex(args.input)
    for count, read in enumerate(inbam.fetch()):
        rGroup = 0
        try:
            rGroup = read.get_tag('RG')
        except:
            print("WARNING: read which doesn't have a RG grouped as bg (background)")
        try:
            totalct[rGroup] += 1
        except KeyError:
            totalct[rGroup] = 1

with open(args.output,'w') as outFile:
    outFile.write('Barcode\tExperiment\tReadCount\n')
    for keys,values in totalct.items():
        outFile.write('{0}\t{1}\t{2}\n'.format(header[keys]['DS'], header[keys]['LB'],values))
