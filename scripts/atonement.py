#!/usr/bin/env python

import argparse
import pysam
import sys
import os
from collections import Counter
from scipy.sparse import save_npz,csc_matrix
from scipy.io import mmwrite
from operator import itemgetter

def is_whatIwant(x,only,start,end):
    if x.has_tag("NH"):
        unique = (x.get_tag("NH") == 1)
    else:
        unique = True
    if only:
        return unique and (end >= x.get_reference_positions()[-1] if x.is_reverse else start<=x.get_reference_positions()[0])
    else:
        return unique 

def chkIndex(fAdd):
    if not os.path.isfile("{}.bai".format(fAdd)):
        print("indexing bam file {}".format(fAdd))
        pysam.index(fAdd)
        if not os.path.isfile("{}.bai".format(fAdd)):
            sys.exit("samtools couldn't creat index file!")


parser = argparse.ArgumentParser(description="A program to do something.")
parser.add_argument('-b', '--input', help='Input bam file', dest='input', required=True)
parser.add_argument('-i', '--index', help='Input readepth Indx table', dest='index', required=True)
parser.add_argument('-w', '--bed', help='Input bedfile to make window map', dest='bed', required=True)
parser.add_argument('-5p', '--5prime',help='count only reads where its 5prime is in the region', dest='only5prime',action="store_true")
parser.add_argument('-n',  '--nonBinary',help='Produce a nonBinary output (this is the same as coverage)', dest='nonBinary',action="store_true")
#Bed format is for not-believer. We can easily specifiy the length of our window and iterate over it.
parser.add_argument('-o', '--output', help='Output location', dest='output', required=True)
parser.set_defaults(only5prime=False)
parser.set_defaults(nonBinary=False)
args = parser.parse_args()

if not(args.input.endswith('sam') or args.input.endswith('bam')):
    raise Exception('input does not look like a sam/bam')

chkIndex(args.input)

# TODO: If RG is in bam you don't need this. Check it with get_tag(self, tag, with_value_type=False)
with open(args.index, 'r') as index:
    cellsdic = {line.strip().split()[1]:index for index,line in enumerate(index) if '@' not in line}

data=[]
rows=[]
cols=[]

notin = 0
allin = 0

with open(args.bed, 'r') as coordinate, \
        pysam.AlignmentFile(args.input, mode='rb') as inbam, \
        open(args.output+".rows",mode='w') as metaFunnyR:
    # TODO: If cellsdic is empty then fill it with RG tags in the header.
    # for tag, val in tags:
    for row, line in enumerate(coordinate):
        c, b, e = line.strip().split()[0:3]
        #If you want unmapped read as well, set until_eof=True
        reads = filter(lambda read: is_whatIwant(read,args.only5prime,int(b),int(e)), inbam.fetch(contig=c, start=int(b), stop=int(e))) 
        counter=Counter(read.qname.split(':')[0] for read in reads)
        
        # In theory I shouldn't count the reads twice if I have a mate pair reads but the next step is going to be paranoid so who cares!!
        for key,value in counter.items():
            allin +=1
            try:
                cols.append(cellsdic[key])
                if args.nonBinary:
                    data.append(value)
                else:
                    data.append(1)
                rows.append(row)
            except KeyError:
                notin +=1
                pass
        metaFunnyR.write("\"{0}-{1}-{2}\"\n".format(c, b, e))

M = row+1
N = len(cellsdic)
print("Data dimmention ({0},{1})".format(M,N))

print("Out of {0} barcodes in the defined region, {1} of them are not listed in the indextable.".format(allin,notin))

with open(args.output+".columns", mode='w') as metaFunnyC:
    for k, v in sorted(cellsdic.items(), key=lambda x: x[1]):
        metaFunnyC.write("\"{0}\"\n".format(k))


spp=csc_matrix((data, (rows, cols)), shape=(M,N))
mmwrite(args.output, spp)
