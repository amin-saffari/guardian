#!/usr/bin/env python

import argparse
import pysam
import sys
import os

parser = argparse.ArgumentParser(description="A program to add RG tag to the given bam file based for the provided barcode list.")
parser.add_argument('-b', '--input', help='Input bam file', dest='input', required=True)
parser.add_argument('-i', '--index', help='Input Indx table', dest='index', required=True)
parser.add_argument('-o', '--output', help='Output location', dest='output', required=True)
parser.add_argument('-r', '--report', help='Print the number of reads falls in each experminent.', dest='report', action="store_true")
args = parser.parse_args()

if not(args.input.endswith('sam') or args.input.endswith('bam')):
    raise Exception('input does not look like a sam/bam')

barcodes = {}
readdic = {} # To capture the number of read in each library

# Read group. Unordered multiple @RG lines are allowed.
RG = {'ID': '',
      'DS': '',
      'LB': '',
      'PL': 'ILLUMINA'}

RG['ID'] = len(barcodes)
RG['DS'] = "NA"
RG['LB'] = "Background" #This cell barcode didn't match the index table which came with the experiment.
RG['PL'] = "ILLUMINA"
barcodes[RG['DS']] = RG
# The plan is to read the barcode file and hash it. Then use this hash table to populate the neccassary files inthis format:
# @RG ID:NUMBER BC:barcode(1st field) LB:concatination of anything but the 1st column(if vailable)
with open(args.index, 'r') as inindex:
    for line in inindex:
        tokens = line.strip().split()
        discription = "NA"
        if len(tokens) >= 2:
            discription = "".join(tokens[1:])
        try:
            RG = RG.copy()
            RG['ID'] = len(barcodes)
            # facepalm!
            # BC is in the sam specification but you don't have its supprt.
            RG['DS'] = tokens[0]
            RG['LB'] = discription
            RG['PL'] = "ILLUMINA"
            barcodes[tokens[0]] = RG
        except KeyError:
            raise Exception('{0} is not uniq!'.format(tokens[0]))

with pysam.AlignmentFile(args.input, mode='rb') as inbam:
    new_header = inbam.header.to_dict()
    new_header['RG'] = list(barcodes.values())
    new_header['PG'].append({'PN':os.path.basename(__file__),'ID':os.path.basename(__file__),'VN':'9bfb6e6','CL': ' '.join(sys.argv[0:])})
    with pysam.AlignmentFile(args.output, mode='wb', header=new_header) as outbam:
        for count, read in enumerate(inbam.fetch()):
            rname = read.qname
            currLib = 0
            currCat = "NA"
            try:
                currLib = barcodes[rname.split(':')[0]]['ID']
                currCat = barcodes[rname.split(':')[0]]['LB']
            except KeyError:
                currLib = 0
                currCat = barcodes['NA']['LB']
            #tags = read.tags
            read.set_tag('RG', currLib)
            #read.tags = tags
            outbam.write(read)
            try:
                readdic[currCat] += 1
            except KeyError:
                readdic[currCat] = 1
if(args.report):
    print("Category\tOccurrence")
    for keys, values in readdic.items():
        print("{0}\t{1}".format(keys, values))
