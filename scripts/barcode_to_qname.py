#!/usr/bin/env python

import os
import sys
import argparse
import gzip

complements = {'A':'T', 'T':'A', 'G':'C', 'C':'G','N':'N'}
def reverse_complement(x):
   xrev = x[::-1]
   xrevcomp = ''.join([complements[z] for z in xrev])
   return xrev

parser = argparse.ArgumentParser(description="A program to manipulate quary-names by barcodes. I1+I2+I4+I3 is the order of the concatination.")
parser.add_argument('-R1','--Read1', help='Input fastq file 1',dest='Read1',required=True)
parser.add_argument('-R2','--Read2', help='Input fastq file 2',dest='Read2',required=True)
parser.add_argument('-O1','--output1', help='Output fastq file 1',dest='output1',required=True)
parser.add_argument('-O2','--output2', help='Output fastq file 2',dest='output2',required=True)
parser.add_argument('-I1','--P7_i7', help='Input Full barcodes of the experiment',dest='I1',required=True)
parser.add_argument('-I2','--P7_PCR', help='Input Full barcodes of the experiment',dest='I2',required=True)
parser.add_argument('-I3','--P5_i5', help='Input Full barcodes of the experiment',dest='I3',required=True)
parser.add_argument('-I4','--P5_PCR', help='Input Full barcodes of the experiment',dest='I4',required=True)
args = parser.parse_args()

with gzip.open(args.I1, 'rb') as I1, \
        gzip.open(args.I2, 'rb') as I2, \
        gzip.open(args.I3, 'rb') as I3, \
        gzip.open(args.I4, 'rb') as I4, \
        gzip.open(args.Read1, 'rb') as if1, \
        gzip.open(args.Read2, 'rb') as if2, \
        open(args.output1, 'w' ) as of1, \
        open(args.output2, 'w' ) as of2:
            """ Read the R1 and compare its qname with I1,I2,I3,I4,read2
                if all are the same then in a same order read the seq part of each indecis
                and yield it as qname of output. The remaining part of the input is going to be print the same.
            """
            counter = 0
            for lif1 in if1:
                counter += 1
                qnameif1 = lif1.strip().decode('UTF-8').split(':')[0]
                qnameif2 = next(if2).strip().decode('UTF-8').split(':')[0]
                qnameI1 = next(I1).strip().decode('UTF-8').split(':')[0]
                qnameI2 = next(I2).strip().decode('UTF-8').split(':')[0]
                qnameI3 = next(I3).strip().decode('UTF-8').split(':')[0]
                qnameI4 = next(I4).strip().decode('UTF-8').split(':')[0]
                if qnameif1 == qnameif2 == qnameI1 == qnameI2 == qnameI4 == qnameI3:
                    seqI1 = next(I1).strip().decode('UTF-8')
                    seqI2 = next(I2).strip().decode('UTF-8')
                    seqI3 = next(I3).strip().decode('UTF-8')
                    seqI4 = next(I4).strip().decode('UTF-8')
                    barcode=seqI1+seqI2+seqI4+seqI3
                    # block for read1
                    of1.write("{0}.{1} {2}:{1}/1\n".format(qnameif1, str(counter), barcode))
                    seqif1 = next(if1).decode('UTF-8').strip()
                    of1.write("{0}\n".format(seqif1))
                    opif1 = next(if1).decode('UTF-8').strip()
                    of1.write("{0}\n".format(opif1))
                    quif1 = next(if1).decode('UTF-8').strip()
                    of1.write("{0}\n".format(quif1))
                    of1.flush()
                    # block for read2
                    of2.write("{0}.{1} {2}:{1}/2\n".format(qnameif2, str(counter), barcode))
                    seqif2 = next(if2).decode('UTF-8').strip()
                    of2.write("{0}\n".format(seqif2))
                    opif2 = next(if2).decode('UTF-8').strip()
                    of2.write("{0}\n".format(opif2))
                    quif2 = next(if2).decode('UTF-8').strip()
                    of2.write("{0}\n".format(quif2))
                    of2.flush()
                    # Ignore the next 2 lines of all indexes
                    # Ugly.
                    for i in range(2):
                        tmp = next(I1).strip()
                        tmp = next(I2).strip()
                        tmp = next(I3).strip()
                        tmp = next(I4).strip()

                else:
                    print("\n{0}\n{1}\n{2}\n{3}\n{4}\n{5}\n".format(qnameif1, qnameif2, qnameI1, qnameI2, qnameI3, qnameI4))
                    sys.exit("query-names of at least one indicies is not equal to the reads query-name")
